window.addEventListener('load', () => {
  [].forEach.call(document.getElementsByClassName('fn-activity'), elm => {
    const priorityId = elm.dataset.id;
    const prioritySelect = elm.getElementsByClassName('fn-priority')[0];
    const deleteBtn = elm.getElementsByClassName('fn-delete')[0];

    deleteBtn.addEventListener('click', async () => {
      const response = await fetch(`/activity/${priorityId}/delete`, {
        method: 'POST',
      });
      if (!response.ok) {
        console.warn('Failed to delete');
        return;
      }
      elm.parentNode.removeChild(elm);
    });

    prioritySelect.addEventListener('change', async () => {
      const response = await fetch(`/activity/${priorityId}/update`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ priority: prioritySelect.value }),
      });
      if (!response.ok) {
        console.warn('Failed to update');
        return;
      }
      window.location.reload();
    });
  });
});
