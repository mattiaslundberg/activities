import pytest
from django.test import TestCase

from activity.models import Activity


class ActivityViewTests(TestCase):
    def test_create_activity(self):
        response = self.client.post("/", data={"name": "My activity", "priority": 20})
        assert response.status_code == 302

        activity = Activity.objects.last()
        assert activity is not None
        assert activity.name == "My activity"
        assert activity.priority == 20

    def test_delete_activity(self):
        activity = Activity.objects.create(name="Some", priority=20)

        response = self.client.post(f"/activity/{activity.id}/delete")
        assert response.status_code == 200

        with pytest.raises(Activity.DoesNotExist):
            activity.refresh_from_db()
