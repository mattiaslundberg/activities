from django.db import models

PRIORITIES = [(10, "Low"), (20, "Medium"), (30, "High")]


class Activity(models.Model):
    name = models.TextField()
    priority = models.IntegerField(choices=PRIORITIES, default=10)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
