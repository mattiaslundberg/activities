import json

from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.views.generic import TemplateView

from activity.forms import ActivityForm
from activity.models import PRIORITIES, Activity


class ActivitiesView(TemplateView):
    template_name = "activities.html"
    form_class = ActivityForm
    initial = {"name": "", "priority": 10}

    def get_activities(self):
        return Activity.objects.all().order_by("-priority")

    def get_context(self):
        return {"activities": self.get_activities(), "priorities": PRIORITIES}

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        context = self.get_context()
        context["form"] = form
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/")

        context = self.get_context()
        context["form"] = form
        return render(request, self.template_name, context)


@require_POST
@csrf_exempt
def delete_activity(request, id):
    activity = get_object_or_404(Activity, id=id)
    count = activity.delete()
    return JsonResponse({"deleted": count})


@require_POST
@csrf_exempt
def set_priority(request, id):
    activity = get_object_or_404(Activity, id=id)
    data = json.loads(request.body)
    activity.priority = data.get("priority", activity.priority)
    activity.save(update_fields=["priority"])
    return JsonResponse({"priority": activity.priority})
